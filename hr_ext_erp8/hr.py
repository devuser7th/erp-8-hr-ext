'''
Created on Sep 1, 2013

@author: Thazin Khaing
'''
from openerp.osv import fields, osv
from datetime import date, datetime
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from dateutil.relativedelta import * 
from openerp import tools

class hr_contract(osv.osv):
    _inherit = 'hr.contract'
    _columns = {
               'well_fare': fields.float('Well Fare Allowance'),
               'personal_tax': fields.float('Tax'),
                }
    
hr_contract()



class hr_employee(osv.osv):
    _inherit = 'hr.employee'
    _columns = {
                'spouse_name':fields.char('Spouse Name'),
                'under_eighteen_single':fields.integer('Under Eighteen Single'),
                'over_eighteen_student':fields.integer('Over Eighteen Student'),
                'work_experience':fields.char('Work Experience year(s)'),
                'is_depend':fields.boolean('Spouse Is Dependent'),
              
                }
    
    def on_change_marital_status(self, cr, uid, ids, marital, context=None):
        if marital == 'married':
            print 'marital status is ....', marital
        
hr_employee()


class hr_employee_graph_view(osv.osv):
    _name = 'hr.employee.graph.view'
    _auto = False
    _columns = {
                'name_related': fields.char('Name'),
                'gender': fields.char('Gender'),
                'date_start': fields.date('Start Date'),
                'date_end': fields.date('End Date'),
                
                'trial_date_start': fields.date('Trial Start Date'),
                'trial_date_end': fields.date('Trial End Date'),
                'marital': fields.char('Marital'),
                'children': fields.char('Children'),
                'place_of_birth': fields.char('Place of Birth'),
                'birthday': fields.date('Birthday'),
                
                'name': fields.char('Department Name'),
                'active': fields.char('Active'),
                'job_id': fields.char('Job'),
                'work_experience': fields.char('Work Experience'),               
                }
    def init(self, cr):
        
        tools.drop_view_if_exists(cr, 'hr_employee_graph_view')
        cr.execute("""
            CREATE or REPLACE view hr_employee_graph_view as (

            SELECT
             min(HE.id) As id, 
             HE.name_related,
             HE.gender,
             HC.date_start,
             HC.date_end,
             HC.trial_date_end,
             HC.trial_date_start,
             HE.marital,
             HE.children,
             HE.place_of_birth,
             HE.birthday,
             HD.name,
             HE.job_id,
             RR.active,
             HE.work_experience

            
            from 
            hr_employee HE,
            hr_contract HC,
            hr_department HD,
            resource_resource RR
            
            WHERE 
            HE.id = HC.employee_id
            AND 
            HE.department_id= HD.id
            AND
            HE.resource_id = RR.id
            
            GROUP BY
            HE.name_related,
            HE.gender,
            HC.date_start,
            HC.date_end,
            HC.trial_date_end,
            HC.trial_date_start,
            HE.marital,
            HE.children,
            HE.place_of_birth,
            HE.birthday,
            HD.name,
            HE.job_id,
            RR.active,
            HE.work_experience           
            )
        """)

